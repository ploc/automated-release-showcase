# [1.2.0](https://framagit.org/ploc/automated-release-showcase/compare/v1.1.0...v1.2.0) (2022-01-14)


### Features

* add content for gitlab release ([96ecdfb](https://framagit.org/ploc/automated-release-showcase/commit/96ecdfb8cbe90d9701bc2ec3b6b3232e79714a85))

# [1.1.0](https://framagit.org/ploc/automated-release-showcase/compare/v1.0.0...v1.1.0) (2022-01-14)


### Features

* add content for git release ([63a0011](https://framagit.org/ploc/automated-release-showcase/commit/63a0011c6380eef1a4872d04a20e5fcc0d5ee1f0))

# 1.0.0 (2022-01-11)


### Features

* add content for local release ([764075b](https://framagit.org/ploc/automated-release-showcase/commit/764075b3b778c1f982fc61e383b3feb768f4a90e))
* initial commit ([bb11a3d](https://framagit.org/ploc/automated-release-showcase/commit/bb11a3d6da722c679d6c63262e29bec29312f716))
